# MakeMKV_register_beta

Updates the MakeMKV beta registration key from the Forum.

https://www.makemkv.com/forum/

## Installation

No installation required.

## Usage

Run the playbook as user:

ansible-playbook key_update.yml
